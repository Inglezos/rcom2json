/*!
 * \file kcli_impl.h
 * \brief
 *    bin2json and json2bin converter command line interface implementation header
 */
#ifndef __kcli_impl_h__
#define __kcli_impl_h__

#ifdef __cplusplus
extern "C" {
#endif

#include <toolbox.h>
#include <rcom.h>

/*
 * =========== User defines ===========
 */
#define  KCLI_PATH_MAX_SIZE         (128)
#define  KCLI_IO_MAX_STREAM_SIZE    (32768UL)
#define  GSM2M_MAX_CON_DEVICES      (15)
#define  ACQUIRE_MAX_ITEMS    		(8)

/*
 * ============= data types ===========
 */

/*!
 * CLI functionality enumerator
 */
typedef enum {
   FUN_NONE = 0,  //!< FUN_NONE:    No Functionality (error)
   FUN_B2J,       //!< FUN_B2J:
   FUN_J2B        //!< FUN_J2B:
}kcli_functionality_en;

/*!
 * CLI file types
 */
typedef enum {
   FT_NONE=0,			//!< FT_NONE:  There is no required file type (error)
   FT_NETSTATUS 	=1,		//!< FT_NETSTATUS: The required file is a netstatus file
   FT_SETTINGS 		=2,		//!< FT_SETTINGS: The required file is a general settings file
   FT_LOG 			=3,		//!< FT_LOG: The required file is a log file
   FT_GSM_SETTINGS  =4,	//!< FT_GSM_SETTINGS: The required file is a gsm settings file
   FT_ALL 			=5
}kcli_file_types_en;


typedef enum {
	INVALID,
	READ,
	WRITE
}kcli_mode_en;


/*!
 * Acquire data items enumerator
 */
typedef enum {
   ACQ_LOG        = 0x01 <<0, //!< ACQ_LOG:     Device's log
   ACQ_ERROR      = 0x01 <<1, //!< ACQ_ERROR:   Device's errors
   ACQ_SETTINGS   = 0x01 <<2, //!< ACQ_SETTINGS:Device's settings
   ACQ_CHANNEL    = 0x01 <<3  //!< ACQ_CHANNEL: Device's channels data
}acquire_en;


/*!
 * Output type
 */
typedef enum {
   OUT_VALVE = 0,    //!< Latch type output (pulse open - pulse close)
   OUT_RELAY         //!< Momentary type output
} output_en;


/*!
 * Acquire data value containing all the requested items
 */
typedef union {
   acquire_en  en;
   uint8_t     val;
} acquire_t;

/*!
 * CPU sleep policy enumerator
 */
typedef enum {
   IDLE_SLEEP =0,//!< IDLE_SLEEP
   IDLE_RUN      //!< IDLE_RUN
}run_policy_en;


/*!
 * CLI input data type
 */
typedef struct {
   kcli_functionality_en
                  func;          //!< Select functionality
   kcli_file_types_en
                  ft;            //!< Select file type

   uint8_t        verb;

   kcli_mode_en
   	   	   	   	  mode;			 //!< the mode is READ or WRITE

   char           input[KCLI_PATH_MAX_SIZE];    //!< Path to input file
   char           output[KCLI_PATH_MAX_SIZE];   //!< Path to output file
   rtp_id_t       did;
   uint8_t        have_did;
} kcli_input_t;



/*
 * ============= Water intake file data =============
 */

/*
 * ============= General ===============
 */
#define  _ST_TIME_BASE_FREQ      (1000)
#define  _READKEY_TRIES          (6)
#define  _SYSTEM_STARTUP_TRIES   (6)
#define  _UI_BALLOON_TIME        (_sec(2))   // sec
#define  _UI_MENU_TIMEOUT_FACTOR (6)   //!< How many times increased the display timeout when on menu

/*
 * ========== Water intake data types ==========
 */
#if WI_USE_DOUBLE_PRECISION == 1
typedef  double         fp_data_t;     //!< Floating point data for meter. Use double for floating point data
typedef  iir_li_d_t     iir_fp_t;      //!< Leaky integrator data for meter. Double precision.
#else
typedef  float          fp_data_t;     //!< Floating point data for meter. Use float for floating point data
typedef  iir_li_f_t     iir_fp_t;      //!< Leaky integrator data for meter. Single precision.
#endif   /* #if WI_USE_DOUBLE_PRECISION == 1 */

#define  CID_SIZE          (8)      /*!< Card ID size equals 1-wire ID size [bytes] */
#define  DID_SIZE          (12)     /*!< Device ID size equals CPU ID size [bytes] */

/*!
 * Valve output operation enumerator
 */
typedef enum {
   WI_VALVE_OFF=0,     /*!< Valve output OFF */
   WI_VALVE_ON,        /*!< Valve output ON */
} wi_valve_cmd_en;


/*
 * ==========   Application data ============
 */

extern const aes_t   root;                /*!< THE user */
extern byte_t        _romID [CID_SIZE];   /*!< Temporary ROMID buffer */
extern const uint8_t _zeroCID [CID_SIZE]; /*!< Zero CID to use for logging operations */

/*!
 * Water intake return type
 */
typedef enum {
   WI_OK,            /*!< Success */
   WI_IO_ERROR,      /*!< IO Error */
   WI_AUTH_ERROR     /*!< Authentication error */
} wi_result_en;

/*!
 * Payment method enumerator
 */
typedef enum {
   WI_PM_NONE,    //!< No payment methode
   WI_PM_VOLUME,  //!< Volume based payment
   WI_PM_ENERGY,  //!< Current consumption based payment
   WI_PM_TIME,    //!< Time based payment
   WI_PM_VM2TIME, //!< Time based payment converted from volume, on sensor error
   WI_PM_KWH2TIME //!< Time based payment converted from energy, on sensor error
} wi_pm_en;

/*!
 * Measurement mode enumerator
 */
typedef enum {
   WI_MEAS_ABSOLUT=0,   /*!<
                         * Absolut measurement:
                         * In each IRQ callback we:
                         * 1) Calculate the 1st derivative of measured data
                         * 2) update the actual measured data value
                         */
   WI_MEAS_RUN_ABSOLUT, /*!<
                         * Run Absolut measurement:
                         * In each IRQ callback we:
                         * 1) Calculate the 1st derivative of measured data
                         * 2) we calculate the actual measured data value from Integral of
                         *    1st derivative
                         * 3) We dont follow Sleep policy until the end of irrigation
                         */
   WI_MEAS_SAMPLE       /*!<
                         * Sampling measurement:
                         * In each IRQ callback we:
                         * 1) Calculate the 1st derivative of measured data
                         * 2) we calculate the actual measured data value from Integral of
                         *    1st derivative
                         */
} wi_mm_en;

/*!
 * Event enumerator
 */
typedef enum {
   EV_BOOT=0,              //!< Device boot event
   EV_POWER_DOWN,          //!< Device power down

   EV_ADMIN_LOGIN = 0x10,  //!< Admin login
   EV_USER_LOGIN,

   EV_BEGIN = 0x20,        //!< Consumption-Valve on
   EV_END,                 //!< Consumption-Valve off
   EV_CREDITS,             //!< Credits event
   EV_LOCKED,              //!< Locked even
   EV_UNLOCKED,            //!< Unlocked event

   EV_ERROR_CARD_IO = 0x80,//!< User key IO error
   EV_ERROR_SENSOR,        //!< Sensor error
   EV_ERROR_CONS_ON,       //!< Valve On error
   EV_ERROR_CONS_OFF,      //!< Valve Off error
   EV_ERROR_AUTH,          //!< Auth error
   EV_ERROR_VALVE_VOLT,    //!< Valve voltage error
   EV_ERROR_BATTERY,       //!< Battery Error
   EV_ERROR_MEMORY,        //!< Memory Error
   EV_ERROR_SYSTEM,        //!< System Error
   EV_ERROR_EXT,           //!< External Error

} wi_event_en;


/*!
 * credit type enumerator
 */
typedef enum {
   CR_LIT = 0,
   CR_KWH,
   CR_TIME
} credits_en;

/*!
 * credits data type
 */
typedef struct {
   int32_t  lit;     /*!< The volume credits in the card */
   int32_t  kwh;     /*!< The energy credits in the card */
   time_t   t;       /*!< The time credits in the card */
} credits_t;
extern const credits_t   _zero_credits;   /*!< Zero credits to use for logging operations */


/*!
 * Volume units enumerator
 */
typedef enum {
   VM_LIT = 1,    //!< Liter
   VM_M3 = 1000   //!< Cubiq meter
}vm_units_en;

/*!
 * Energy units enumerator
 */
typedef enum {
   NRG_KWH = 1,      //!< Kilowatthours
   NRG_MWH = 1000    //!< Megawatthours
}nrg_units_en;

/*!
 * Time units enumerator
 */
typedef enum {
   TIME_SEC = 1,     //!< Seconds
   TIME_MIN = 60,    //!< Minutes
   TIME_HOUR= 3600   //!< Hours
}time_units_en;

/*!
 * Log id enumerator to seperate device id, or user-card id pair
 * when used in \ref log_id_t
 */
typedef enum {
   LOG_T_USER=0,  /*!< The ID type is for UID-CID pair*/
   LOG_T_DEVICE   /*!< THe ID type is for DID */
} log_id_en;

/*!
 * Log ID data type
 */
typedef struct {
   log_id_en   type;                   /*!< Mark which ID to use inside the union */
   union {
      struct {
         uint32_t    UID;              /*!< Use ID */
         uint8_t     CID [CID_SIZE];   /*!< Card ID */
      } user;                          /*!<
                                        * The User part of union
                                        * In this configuration we save both User and Card IDs
                                        */
      struct {
         uint8_t     DID [DID_SIZE];   /*!< Device ID */
      } device;                        /*!<
                                        * The device part of union
                                        * In this configuration we only save the Device ID
                                        */
   }id;                                /*!< The ID (device or user) involved in log event */
} log_id_t;

/*!
 * The log data type. This is the actual object saved
 */
typedef struct {
   time_t      timestamp;     /*!< UNIX timestamp */
   wi_event_en event;         /*!< The event that loged */
   log_id_t    id;            /*!< The id of the "person" the event is assigned */
   credits_t   credits;       /*!< The amount of credits involved */
} log_t;

/*!
 * Energy related settings
 */
typedef struct {
   int32_t     ppkwh;      /*!< Instalation's pulse per kWh */
   fp_data_t   def_kwhps;  /*!<
                            * Default kWh per second to charge on metering failure
                            * This value is used when the device operates in \ref WI_PM_ENERGY
                            * and the \ref wi_error.sensor is set
                            */
   fp_data_t    kwhps_min; /*!< Minimum limit for calculated kwhps rate */
   fp_data_t    kwhps_max; /*!< Maximum limit for calculated kwhps rate */
   nrg_units_en units;     /*!< The units for UI */
}kwh_settings_t;

/*!
 * volume related settings
 */
typedef struct {
   fp_data_t   lpp;        /*!< Instalation's liter per pulse per [lit] */
   fp_data_t   def_lps;    /*!<
                            * Default lit per second to charge on metering failure
                            * This value is used when the device operates in \ref WI_PM_VOLUME
                            * and the \ref wi_error.sensor is set
                            */
   fp_data_t   lps_min;    /*!< Minimum limit for calculated lps rate */
   fp_data_t   lps_max;    /*!< Maximum limit for calculated lps rate */
   vm_units_en units;      /*!< The units for UI */
}vm_settings_t;

/*!
 * Time related settings
 */
typedef struct {
   time_units_en  units;   /*!< The units for UI */
}time_settings_t;

/*!
 * SLeep policy enumerator
 */
typedef enum {
   HAL_IDLE_SLEEP = 1,  //!< Sleep when idle
   HAL_IDLE_RUN = 2     //!< Run when idle
}hal_idle_policy_en;

#define WI_PASS_SIZE    (0x10)

/*!
 * Password type
 */
typedef struct {
   uint8_t  device[WI_PASS_SIZE];  /*!< The admin's auth-key password */
   uint8_t  admin [WI_PASS_SIZE];  /*!< The device's auth-key password */
} password_t;

/*!
 * App Settings structure
 */
typedef struct {
   password_t     pass;                /*!< auth-key passwords */
   wi_pm_en       paymethode;          /*!< Installation's Payment method */
   wi_mm_en       mesmode;             /*!< Installation's Measurement mode */
   vm_settings_t  vm;                  /*!< volume charge related settings */
   kwh_settings_t nrg;                 /*!< Energy charge related settings */
   time_settings_t t;                  /*!< Time charge related settings */
   credits_t      du;                  /*!< UI's step unit */
   uint8_t        autoconv;            /*!< Indicates if a reading sensor error will lead to "convert to time" payment */
   time_t         disp_timeout;		   /*!< Display error timeout */
   time_t         sens_timeout;        /*!< Sensor error timeout */
   time_t         relay_timeout;       /*!< Relay output timeout */
   time_t         sample_time;         /*!< Sampling time when in sampling measurement mode */
   time_t         sample_interval;     /*!< Interval between samples when in sampling mode */
   time_t         sample_delay;        /*!< Time delay before first sample when irrigaion begins */
} app_settings_t;

/*!
 * HAL Settings structure
 */
typedef struct {
   output_en      output_type;		   /*!< The output type.*/
   uint8_t        sensor_pwr;          /*!< Set if the sensor input needs power */
   uint8_t        BUS_vID;             /*!< The RCOM bus virtual ID of the device */
   uint8_t        BUS_bunch_send;      /*!< How many rcom_send we can send in a row */
   fp_data_t      volt_valve_low;      /*!< Valve lower voltage, start v12 domain below that value */
   fp_data_t      volt_valve_high;     /*!< Valve desired voltage */
   fp_data_t      volt_bat_low;        /*!< Battery minimum voltage */
   fp_data_t      volt_bat_warn;       /*!< Battery warning voltage */
//   fp_data_t      volt_bat_high;       /*!< Battery maximum voltage */
   clock_t        time_valve_on;       /*!< Valve on command time */
   clock_t        time_valve_off;      /*!< Valve off command time */
   clock_t        timeout_valve;       /*!< Valve voltage timeout */
   hal_idle_policy_en sleep_policy;    /*!< Idle sleep policy */
} hal_settings_t;


/*!
 * Settings structure-file
 */
typedef struct {
   uint8_t           magic;   /*!< HAL settings EE magic number */
   uint32_t          SID;     /*!< HAL Settings ID, to link in DB */
   app_settings_t    app;     /*!< App Settings structure */
   hal_settings_t    hal;     /*!< HAL Settings structure */
}settings_t;
extern settings_t settings;




/*!
 * Lock policy
 */
typedef struct {
    uint8_t       sens2usr    :1;   /*!< Lock the user on sensor error */
    uint8_t       sens2device :1;   /*!<
                                     * Lock the device on sensor error
                                     * If not the device start to provide based on time
                                     * this is done through the \ref user_db[].pps of each user.
                                     * If that is not available then we relay on \ref settings.vm.def_m3ps
                                     * or \ref settings.kwh.def_kwhps
                                     */
    uint8_t       auth2key    :1;   /*!< Lock the key on key-auth error */
    uint8_t       auth2usr    :1;   /*!< Lock the usr on key-auth error */
    uint8_t       auth2device :1;   /*!< Lock the device on device-auth error */
} lock_policy_t;

/*!
 * Log policy
 */
typedef struct {
   uint8_t        login       	:1;   /*!< Log login event */
   uint8_t        lock        	:1;   /*!< Log lock event */
   uint8_t        begin       	:1;   /*!< Log begin event */
   uint8_t        end         	:1;   /*!< Log end event */
   uint8_t        credits  	  	:1;   /*!< Log credit event */
   uint8_t        power_up    	:1;   /*!< Log power up event */
   uint8_t        power_dwn   	:1;   /*!< Log power down event */
   uint8_t        er_card_io  	:1;   /*!< Log card IO error event */
   uint8_t        er_sensor   	:1;   /*!< Log sensor error event */
   uint8_t        er_cons_on  	:1;   /*!< Log consumption on error event */
   uint8_t        er_cons_off 	:1;   /*!< Log consumption off error event */
   uint8_t        er_auth     	:1;   /*!< Log authentication error event */
   uint8_t        er_valve_volt	:1;   /*!< Log Valve domain voltage error event */
   uint8_t        er_battery  	:1;   /*!< Log battery error event */
   uint8_t        er_memory   	:1;	  /*!< Log memory error event */
   uint8_t        er_system   	:1;   /*!< Log system error event */
} log_policy_t;


typedef struct {
   uint8_t        magic;      /*!< policy EE magic number */
   uint32_t       PID;        /*!< Policy ID, to link in DB */
   lock_policy_t  lock;
   log_policy_t   log;
} policy_t;
extern policy_t policy;

#define  WI_EE_MAGIC       (0x55)


/*!
 * Access policy data type
 */
typedef union {
   struct {
      uint8_t  error       :1;   /*!< Access to error acknoledge */
      uint8_t  setup       :1;   /*!< Access to setup menu items */
      uint8_t  meas_pay    :1;   /*!< Access to measurement and payment related menu */
      uint8_t  power       :1;   /*!< Access to power related menu */
      uint8_t  timing      :1;   /*!< Access to timing related menu */
      uint8_t  log         :1;   /*!< Access to log policy related menu */
      uint8_t  lock        :1;   /*!< Access to lock policy related menu  */
      uint8_t  pass        :1;   /*!< Access to password setup */
   }flag;
   uint8_t flags;
} access_policy_t;


/*!
 * User key file
 */
typedef struct {
   uint32_t    magic;            /*!< File magic number to check validation of decription */
   uint32_t    UID;              /*!< The User ID who owns the file/card */
   uint8_t     CID [CID_SIZE];   /*!< The Card ID for wich the file is for */
   credits_t   credits;          /*!< The credits in the card */
   credits_t   def_credits;      /*!< The default credits to consume in every operation */
   access_policy_t access;       /*!< Access policy of the user */
   uint8_t     locked;           /*!< Lock flag of the file */
} mdata_t;


typedef struct {
	settings_t settings;
	policy_t policy;
} Gen_Settings_t;
extern Gen_Settings_t Gen_Settings;


/*!
 * Application's settings data struct
 */
typedef struct {
   uint8_t     		vID;                 // [0-15]
   run_policy_en  	run_policy;
   time_t      		report_interval;     // [sec]
   time_t      		inc_time_window;     // [sec]
   char        		gsm_pin[5];          // GSM card pin
   char        		apn[50];             // Access point name
   char        		protocol[4];         // Protocol TCP or UDP
   char        		server_domain[50];   // Server domain name
   char        		server_port[6];      // Server port
   char        		time_server[50];     // Time server domain
   acquire_t   		acq;
//   char				phone_number[20];			 // Emergency phone number for error logging in SMS format.
} GSM_settings_t;
extern GSM_settings_t GSM_settings;


typedef struct {
   rtp_id_t router;
   rtp_id_t dev[GSM2M_MAX_CON_DEVICES];
}net_status_t;
extern net_status_t netstatus;


typedef struct {
   union {
	   uint8_t  		buffer [KCLI_IO_MAX_STREAM_SIZE];
	   Gen_Settings_t	Gen_Settings;
	   log_t 			log [KCLI_IO_MAX_STREAM_SIZE / sizeof (log_t)];
	   net_status_t		netstatus;
	   GSM_settings_t	GSM_settings;
   } data;
   uint32_t    cursor;
} UnivStruct_t;
extern UnivStruct_t		UnivStruct;


extern char  JsonFile [KCLI_IO_MAX_STREAM_SIZE];
typedef struct {
   uint8_t     buffer [KCLI_IO_MAX_STREAM_SIZE];
   uint32_t    cursor;
} UnivPackage_t;
extern UnivPackage_t     UnivPackage;

extern kcli_input_t   Input;




/*!
 * Seed file
 */
typedef struct {
   uint32_t    magic;
   uint8_t     CID [CID_SIZE];
} seed_t;

/*!
 * Pass file
 */
typedef struct {
   uint32_t    magic;
   password_t  pass;
} pass_t;

#define  WI_FILE_MAGIC        (0x005EED00)


/*
 * =============== Users =============
 */
#define  UID_NONE             (0)      //!< The ID of "No user"
#define  UID_ROOT             (1)      //!< The ID of root user
#define  UID_SUPER            (2)      //!< The ID of Super user
#define  UID_ADMIN_MAX        (99)     //!< The maximum Admin ID
#define  UID_USER_MIN         (100)    //!< The minimum User ID

/*! Absolut value macro */
#define _abs(_v)              ((_v < 0) ? -_v : _v)



#ifdef __cplusplus
}
#endif

#endif /* #ifndef __kcli_impl_h__ */




