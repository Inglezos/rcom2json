/*!
 * \file ui.h
 * \brief
 *    kEY command line interface UI
 */
#ifndef __ui_h__
#define __ui_h__

#ifdef __cplusplus
extern "C" {
#endif

#include <kcli_impl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>

/*
 * ========== UI Public API ============
 */
int parse_args (kcli_input_t *in, int argc, char *argv[]);

#ifdef __cplusplus
}
#endif

#endif /* #ifndef __ui_h__ */
