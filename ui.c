#include "ui.h"

/*!
 * Program documentation.
 */
static const char doc[] =
"key_cli\n\
   A client CLI to read and write water intake binary data streams to json files (log files) and vice versa (settings, log and netstatus data files).\n\n\n";

/*!
 * A description of the arguments we accept.
 */

static const char build[] = "[FULL] version 5.2\n";

static const char args_doc[] =
"Water_bin2json -x B2J -t type -i data_in -o [data_out] [-v]\n\n\
   -x B2J, --Binary (rcom) to JSON\n\
      Converts input binary data to JSON output data.\n\n\
   -t, --type\n\
      Select the data file type to be converted to JSON format;\n\
      Available types are: {SETTINGS, LOG, NETSTATUS}\n\n\
   -i, --input <DATA_SOURCE>\n\
	  DATA_SOURCE: Pointer/Path to data to use as input for the program.\n\n\
   -o, --output <DATA_DEST>\n\
      DATA_DEST: Pointer/Path to data to use as output of the program.\n\
      If none provided stdout is selected\n\n\
   -v --verbose\n\
      Increase printed message information\n\n\
\
 Water_bin2json -x J2B -m mode -d <ID> -t type [-i data_in] -o [data_out] [-v]\n\n\
   -x J2B, --JSON to Binary (rcom)\n\
      Converts input JSON data to binary output data.\n\n\
   -t, --type\n\
      Select the file type to be converted to binary (rcom) format;\n\
      Available types are: {SETTINGS, LOG, NETSTATUS}\n\n\
   -m, --mode\n\
	  Sets the read (READ) or write (WRITE) mode for the data.\
   -d, --destination ---> the 12 byte ID of the desired device.\n\
	  Sets the ID for the header of the packets to be sent.\n\n\
   -i, --input <DATA_SOURCE>\n\
	  DATA_SOURCE: Pointer/Path to data to use as input for the program.\n\n\
   -o, --output <DATA_DEST>\n\
      DATA_DEST: Pointer/Path to data to use as output of the program.\n\
      If none provided stdout is selected\n\n\
   -v --verbose\n\
      Increase printed message information\n\n\n\
\
 Water_bin2json -h\n\n\
   -h, --help\n\
      Print this menu end exit\n\n\
\
 Water_bin2json -b\n\n\
   -b, --build\n\
      Print build version end exit\n\n\
\
   Examples:\n\n\n\
\
   *****	<SETTINGS> BINARY (rcom) ---> <SETTINGS> JSON	*****\n\n\
  Water_bin2json -x B2J -t SETTINGS -i C:\\file [-o C:\\file]\n\
      Read incoming SETTINGS binary (rcom) data file, convert it to JSON format and save it to file locally or print it to stdout.\n\
\
   *****	<SETTINGS> JSON ---> <SETTINGS> BINARY (rcom)	*****\n\n\
   Water_bin2json -x J2B -t SETTINGS -m WRITE -d 1:2:3:4:5:6:7:8:9:10:11:12 -i C:\\file [-o C:\\file]\n\
		>> Read incoming SETTINGS JSON data file, convert it to binary (rcom) format (packets) and save it to file locally or print it to stdout\n\n\
   Water_bin2json -x J2B -t SETTINGS -m READ -d 1:2:3:4:5:6:7:8:9:10:11:12 [-o C:\\file]\n\
   	    >> Read incoming SETTINGS JSON data file, convert it to binary (rcom) format (packets) and save it to file locally or print it to stdout\n\n";


/*!
 * CLI short options
 */
const char *short_opt = "x:t:m:i:o:d:vhb";

/*!
 * CLI long options
 */
const struct option long_opt[] = {
   {"conversion",    required_argument,   NULL, 'x'},
   {"type",          required_argument,   NULL, 't'},
   {"mode",        	 required_argument,   NULL, 'm'},
   {"input",         required_argument,   NULL, 'i'},
   {"output",        required_argument,   NULL, 'o'},
   {"destination",   required_argument,   NULL, 'd'},
   {"verbose",       no_argument,         NULL, 'v'},
   {"help",          no_argument,         NULL, 'h'},
   {"build",         no_argument,         NULL, 'b'},
   {NULL,            0,                   NULL, 0}
};

// stdio link with toolbox
int __putchar (char c) { return putchar (c); }
int __getchar (void) { return getchar (); }

#define  _invalid_option()    \
   do {                       \
      fprintf (stderr, "%s: invalid option -- %c\n", argv[0], c);             \
      fprintf (stderr, "Try `%s --help' for more information.\n", argv[0]);   \
      return 1;                                                               \
   } while (0)

/*!
 * \brief
 *    Parse input argument and fill the kcli_input_t struct
 * \param   in    Pointer to \ref kcli_input_t structure to fill
 * \param   argc  The argument count as passed to the main()
 * \param   argv  Argument array as passed to the main()
 * \return  The status of the operation
 *    \arg  0  Success
 *    \arg  1  Fail
 */
int parse_args (kcli_input_t *in, int argc, char *argv[])
{
   int c;
   int did[12];

   while ((c = getopt_long (argc, argv, short_opt, long_opt, NULL)) != -1) {
      switch (c) {
         case -1:       /* no more arguments */

         case 0:        /* long options toggles */
            break;

         case 'x':
        	 if (in->func == FUN_NONE) {
        		 if (!strcmp ((const char*)optarg, (const char*)"B2J"))
        			 in->func = FUN_B2J;
        		 else if (!strcmp ((const char*)optarg, (const char*)"J2B"))
        			 in->func = FUN_J2B;
        	     else {
        	    	 in->func = FUN_NONE;
        	    	 _invalid_option ();
				}
        	 }
			 else {
				in->func = FUN_NONE;
				_invalid_option ();
			 }
			 break;

         case 't':
            if (in->ft == FT_NONE) {
               if (!strcmp ((const char*)optarg, (const char*)"SETTINGS"))
                  in->ft = FT_SETTINGS;
               else if (!strcmp ((const char*)optarg, (const char*)"LOG"))
            	   in->ft = FT_LOG;
               else if (!strcmp ((const char*)optarg, (const char*)"NETSTATUS"))
                   in->ft = FT_NETSTATUS;
               else if (!strcmp ((const char*)optarg, (const char*)"GSM_SETTINGS"))
                   in->ft = FT_GSM_SETTINGS;
               else if (!strcmp ((const char*)optarg, (const char*)"ALL"))
                   in->ft = FT_ALL;
               else {
                  in->ft = FT_NONE;
                  _invalid_option ();
               }
            }
            else {
            	in->ft = FT_NONE;
            	_invalid_option ();
            }
            break;

         case 'm':
			 if (!strcmp ((const char*)optarg, (const char*)"READ"))
				 in->mode = READ;
			 else if (!strcmp ((const char*)optarg, (const char*)"WRITE"))
				 in->mode = WRITE;
			 else {
				 in->mode = INVALID;
				 _invalid_option ();
			}
			break;

         case 'i':   strcpy (in->input, optarg);   break;
         case 'o':   strcpy (in->output, optarg);  break;
         case 'd':
            sscanf (optarg, "%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d",
                    &did[0],
                    &did[1],
                    &did[2],
                    &did[3],
                    &did[4],
                    &did[5],
                    &did[6],
                    &did[7],
                    &did[8],
                    &did[9],
                    &did[10],
                    &did[11]);
            for (int i =0 ; i<12 ; ++i)
               Input.did.octet[i] = (uint8_t)did[i];
            Input.have_did = 1;
            break;
         case 'v':   in->verb = 1;                 break;

         case 'h':
            printf ("%s", doc);
            printf ("%s", args_doc);
            exit (0);

         case 'b':
            printf ("%s", build);
            exit (0);

         case ':':
         case '?':
            fprintf (stderr, "Try `%s --help' for more information.\n", argv[0]);
            return 1;

         default:
            _invalid_option ();
      }
   }
   return 0;
}

