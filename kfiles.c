/*!
 * \file kfiles.c
 * \brief
 *    CLI json file interface
 */


#include "kfiles.h"

/*
 * ============= Public data ===========
 */
UnivStruct_t     UnivStruct;
char             JsonFile [KCLI_IO_MAX_STREAM_SIZE];
UnivPackage_t    UnivPackage;
rtp_t            rtp;
rcom_t           rcom;


/*
 * ========= Static data ==========
 */

static jsmn_parser _parser;
static jsmntok_t   _tokens[512];  /* We expect no more than 512 tokens */

static int _jsoneq (const char *json, jsmntok_t *tok, const char *s);
static int _json_get_primitive (const char *json, jsmntok_t *t, int *iter);
static float _json_get_primitive_float (const char *json, jsmntok_t *t, int *iter);
static int _json_get_string (char *ar, const char *json, jsmntok_t *t, int *iter);
static int _json_get_array (int *ar, int size, const char *json, jsmntok_t *t, int *iter);


/*!
 * \brief
 * 		Destroy json object, setting object's begin and end, equal.
 * \param   tok   Pointer to token array of JSON object
 */
static inline void _json_criple_token (jsmntok_t *t) {
	t->end = t->start;
}

/*!
 * \brief
 *    Seek json string for a string
 * \param   json  Pointer to JSON string to search
 * \param   tok   Pointer to token array of JSON sting
 * \param   s     Pointer to string to search for match
 * \return  The status of the operation
 *    \arg  0  Success
 *    \arg  -1 Fail
 */
static int _jsoneq (const char *json, jsmntok_t *tok, const char *s)
{
   if (tok->type == JSMN_STRING &&
       (int) strlen (s) == tok->end - tok->start &&
       strncmp (json + tok->start, s, tok->end - tok->start) == 0) {
      return 0;
   }
   else if (tok->type == JSMN_OBJECT &&
		    strncmp (json + tok->start, s, 1) == 0) {
	   return 0;
   }
   return -1;
}
static void  _json_skip_nested (const char *json, jsmntok_t *t, int *iter) {
	_json_criple_token (&_tokens[*iter]);
}

static int   _json_get_primitive (const char *json, jsmntok_t *t, int *iter)
{
   int val;
   char tmp[12];
   int s = t[*iter+1].end - t[*iter+1].start;

   _json_criple_token (&_tokens[*iter]);
   memcpy ((void*)tmp, (const void*)(json + t[*iter+1].start), s);
   tmp [s] = 0;
   sscanf (tmp, "%d", &val);
   ++*iter;
   return val;
}

// Parses a float number from the respective JSON object.
static float   _json_get_primitive_float (const char *json, jsmntok_t *t, int *iter)
{
   float val;
   char tmp[12];
   int s = t[*iter+1].end - t[*iter+1].start;

   _json_criple_token (&_tokens[*iter]);
   memcpy ((void*)tmp, (const void*)(json + t[*iter+1].start), s);
   tmp [s] = 0;
   sscanf (tmp, "%f", &val);
   ++*iter;
   return val;
}

static int   _json_get_string (char *ar, const char *json, jsmntok_t *t, int *iter)
{
   int s = t[*iter+1].end - t[*iter+1].start;

   _json_criple_token (&_tokens[*iter]);
   memcpy ((void*)ar, (const void*)(json + t[*iter+1].start), s);
   ar [s] = 0;
   ++*iter;
   return s;
}

static int   _json_get_array (int *ar, int size, const char *json, jsmntok_t *t, int *iter)
{
   int j, s;
   char tmp[12];

   _json_criple_token (&_tokens[*iter]);
   if (t[*iter+1].type == JSMN_ARRAY) {
      for (j=0; j<t[*iter+1].size && j<size; ++j) {
         s = t[*iter+j+2].end - t[*iter+j+2].start;
         memcpy ((void*)tmp, (const void*)(json + t[*iter+j+2].start), s);
         tmp [s] = 0;
         sscanf (tmp, "%d", &ar[j]);
      }
      *iter += t[*iter+1].size + 1;
      return j;
   }
   return 0;
}

/*!
 * \brief
 *    Convert a JSON settings file to binary settings file
 * \param json    Pointer to string containing the file PATH
 * \param size    The size of json input file
 * \param bin     Pointer to binary seed data
 * \return  The status of the operation
 *    \arg  0     Fail
 *    \arg  !0    Success, the number of output converted bytes
 */
static int settings_json2bin (const char *json, size_t size, Gen_Settings_t *bin)
{
   int keys;

   jsmn_init (&_parser);
   if ((keys = jsmn_parse (&_parser, json, size, _tokens, sizeof(_tokens)/sizeof(_tokens[0]))) < 0 ) {
      return 1;
   }

   /* Loop over all keys of the root object */
   for (int i=1; i<keys; ++i) {
	  // magic
	  if (_jsoneq(json, &_tokens[i], "magic") == 0) {
		  bin->settings.magic = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }

	  // SID
	  else if (_jsoneq(json, &_tokens[i], "SID") == 0) {
		  bin->settings.SID = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }

	  // APP Settings
	  else if (_jsoneq(json, &_tokens[i], "pass_device") == 0) {
		  int array_token[WI_PASS_SIZE];
		  _json_get_array(array_token, WI_PASS_SIZE, json, (jsmntok_t *)_tokens, &i);
		  for (int j=0; j<WI_PASS_SIZE; ++j) {
			  bin->settings.app.pass.device[j] = array_token[j];
		  }
	  }

	  else if (_jsoneq(json, &_tokens[i], "pass_admin") == 0) {
		  int array_token[WI_PASS_SIZE];
		  _json_get_array(array_token, WI_PASS_SIZE, json, (jsmntok_t *)_tokens, &i);
		  for (int j=0; j<WI_PASS_SIZE; ++j) {
			  bin->settings.app.pass.admin[j] = array_token[j];
		  }
	  }

	  else if (_jsoneq(json, &_tokens[i], "paymethode") == 0) {
		  bin->settings.app.paymethode = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "mesmode") == 0) {
		  bin->settings.app.mesmode = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "vm_lpp") == 0) {
		  bin->settings.app.vm.lpp = _json_get_primitive_float (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "vm_def_lps") == 0) {
		  bin->settings.app.vm.def_lps = _json_get_primitive_float (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "vm_lps_min") == 0) {
		  bin->settings.app.vm.lps_min = _json_get_primitive_float (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "vm_lps_max") == 0) {
		  bin->settings.app.vm.lps_max = _json_get_primitive_float (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "vm_units") == 0) {
		  bin->settings.app.vm.units = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "nrg_ppkwh") == 0) {
		  bin->settings.app.nrg.ppkwh = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "nrg_def_kwhps") == 0) {
		  bin->settings.app.nrg.def_kwhps = _json_get_primitive_float (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "nrg_kwhps_min") == 0) {
		  bin->settings.app.nrg.kwhps_min = _json_get_primitive_float (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "nrg_kwhps_max") == 0) {
		  bin->settings.app.nrg.kwhps_max = _json_get_primitive_float (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "nrg_units") == 0) {
		  bin->settings.app.nrg.units = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "t_units") == 0) {
		  bin->settings.app.t.units = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "du_lit") == 0) {
		  bin->settings.app.du.lit = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "du_kwh") == 0) {
		  bin->settings.app.du.kwh = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "du_t") == 0) {
		  bin->settings.app.du.t = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "autoconv") == 0) {
		  bin->settings.app.autoconv = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "disp_timeout") == 0) {
		  bin->settings.app.disp_timeout = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "sens_timeout") == 0) {
		  bin->settings.app.sens_timeout = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "relay_timeout") == 0) {
		  bin->settings.app.relay_timeout = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "sample_time") == 0) {
		  bin->settings.app.sample_time = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "sample_interval") == 0) {
		  bin->settings.app.sample_interval = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "sample_delay") == 0) {
		  bin->settings.app.sample_delay = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }

	  // HAL Settings
	  else if (_jsoneq(json, &_tokens[i], "output_type") == 0) {
		  bin->settings.hal.output_type = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "sensor_pwr") == 0) {
		  bin->settings.hal.sensor_pwr = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "BUS_vID") == 0) {
		  bin->settings.hal.BUS_vID = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "BUS_bunch_send") == 0) {
		  bin->settings.hal.BUS_bunch_send = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "volt_valve_low") == 0) {
		  bin->settings.hal.volt_valve_low = _json_get_primitive_float (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "volt_valve_high") == 0) {
		  bin->settings.hal.volt_valve_high = _json_get_primitive_float (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "volt_bat_low") == 0) {
		  bin->settings.hal.volt_bat_low = _json_get_primitive_float (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "volt_bat_warn") == 0) {
		  bin->settings.hal.volt_bat_warn = _json_get_primitive_float (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "time_valve_on") == 0) {
		  bin->settings.hal.time_valve_on = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "time_valve_off") == 0) {
		  bin->settings.hal.time_valve_off = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "timeout_valve") == 0) {
		  bin->settings.hal.timeout_valve = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "sleep_policy") == 0) {
		  bin->settings.hal.sleep_policy = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }

	  // Policy Settings
	  else if (_jsoneq(json, &_tokens[i], "policy_magic") == 0) {
		  bin->policy.magic = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "PID") == 0) {
		  bin->policy.PID = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "lock_sens2usr") == 0) {
		  bin->policy.lock.sens2usr = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "lock_sens2device") == 0) {
		  bin->policy.lock.sens2device = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "lock_auth2key") == 0) {
		  bin->policy.lock.auth2key = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "lock_auth2usr") == 0) {
		  bin->policy.lock.auth2usr = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "lock_auth2device") == 0) {
		  bin->policy.lock.auth2device = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_login") == 0) {
		  bin->policy.log.login = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_lock") == 0) {
		  bin->policy.log.lock = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_begin") == 0) {
		  bin->policy.log.begin = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_end") == 0) {
		  bin->policy.log.end = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_credits") == 0) {
		  bin->policy.log.credits = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_power_up") == 0) {
		  bin->policy.log.power_up = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_power_dwn") == 0) {
		  bin->policy.log.power_dwn = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_er_card_io") == 0) {
		  bin->policy.log.er_card_io = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_er_sensor") == 0) {
		  bin->policy.log.er_sensor = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_er_cons_on") == 0) {
		  bin->policy.log.er_cons_on = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_er_cons_off") == 0) {
		  bin->policy.log.er_cons_off = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_er_auth") == 0) {
		  bin->policy.log.er_auth = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_er_valve_volt") == 0) {
		  bin->policy.log.er_valve_volt = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_er_battery") == 0) {
		  bin->policy.log.er_battery = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_er_memory") == 0) {
		  bin->policy.log.er_memory = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "log_er_system") == 0) {
		  bin->policy.log.er_system = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else {
		 break;
	  }
   }

   return (sizeof(Gen_Settings_t));
}

/*!
 * \brief
 *    Convert a binary settings file to JSON settings file.
 *    This function will convert the current packet from bin to JSON format.
 * \param json    Pointer to string containing the file PATH
 * \param bin     Pointer to binary seed data
 * \return
 *    \arg  0     Fail
 *    \arg  !0    Success, the number of output converted bytes
 */
static int settings_bin2json (char *json, const Gen_Settings_t *bin)
{
   int i=0;

   i += sprintf (&json[i], "{\r\n");

   i += sprintf (&json[i], "		\"magic\" : %u,\r\n", bin->settings.magic);

   i += sprintf (&json[i], "		\"SID\" : %u,\r\n", bin->settings.SID);



   i += sprintf (&json[i], "		\"pass_device\" : [");
   for (int j=0; j<sizeof(uint8_t)*(WI_PASS_SIZE-1); ++j) {
	   i += sprintf (&json[i], "%d, ", bin->settings.app.pass.device[j]);
   }
   i += sprintf (&json[i], "%d", bin->settings.app.pass.device[sizeof(uint8_t)*(WI_PASS_SIZE-1)]);
   i += sprintf (&json[i], "],\r\n");



   i += sprintf (&json[i], "		\"pass_admin\" : [");
   for (int j=0; j<sizeof(uint8_t)*(WI_PASS_SIZE-1); ++j) {
	   i += sprintf (&json[i], "%d, ", bin->settings.app.pass.admin[j]);
   }
   i += sprintf (&json[i], "%d", bin->settings.app.pass.admin[sizeof(uint8_t)*(WI_PASS_SIZE-1)]);
   i += sprintf (&json[i], "],\r\n");



   i += sprintf (&json[i], "		\"paymethode\" : %d,\r\n", bin->settings.app.paymethode);

   i += sprintf (&json[i], "		\"mesmode\" : %d,\r\n", bin->settings.app.mesmode);

   i += sprintf (&json[i], "		\"vm_lpp\" : %f,\r\n", bin->settings.app.vm.lpp);

   i += sprintf (&json[i], "		\"vm_def_lps\" : %f,\r\n", bin->settings.app.vm.def_lps);

   i += sprintf (&json[i], "		\"vm_lps_min\" : %f,\r\n", bin->settings.app.vm.lps_min);

   i += sprintf (&json[i], "		\"vm_lps_max\" : %f,\r\n", bin->settings.app.vm.lps_max);

   i += sprintf (&json[i], "		\"vm_units\" : %d,\r\n", bin->settings.app.vm.units);

   i += sprintf (&json[i], "		\"nrg_ppkwh\" : %u,\r\n", bin->settings.app.nrg.ppkwh);

   i += sprintf (&json[i], "		\"nrg_def_kwhps\" : %f,\r\n", bin->settings.app.nrg.def_kwhps);

   i += sprintf (&json[i], "		\"nrg_kwhps_min\" : %f,\r\n", bin->settings.app.nrg.kwhps_min);

   i += sprintf (&json[i], "		\"nrg_kwhps_max\" : %f,\r\n", bin->settings.app.nrg.kwhps_max);

   i += sprintf (&json[i], "		\"nrg_units\" : %d,\r\n", bin->settings.app.nrg.units);

   i += sprintf (&json[i], "		\"t_units\" : %d,\r\n", bin->settings.app.t.units);

   i += sprintf (&json[i], "		\"du_lit\" : %u,\r\n", bin->settings.app.du.lit);

   i += sprintf (&json[i], "		\"du_kwh\" : %u,\r\n", bin->settings.app.du.kwh);

   i += sprintf (&json[i], "		\"du_t\" : %d,\r\n", (int)bin->settings.app.du.t);

   i += sprintf (&json[i], "		\"autoconv\" : %d,\r\n", bin->settings.app.autoconv);

   i += sprintf (&json[i], "		\"disp_timeout\" : %d,\r\n", (int)bin->settings.app.disp_timeout);

   i += sprintf (&json[i], "		\"sens_timeout\" : %d,\r\n", (int)bin->settings.app.sens_timeout);

   i += sprintf (&json[i], "		\"relay_timeout\" : %d,\r\n", (int)bin->settings.app.relay_timeout);

   i += sprintf (&json[i], "		\"sample_time\" : %d,\r\n", (int)bin->settings.app.sample_time);

   i += sprintf (&json[i], "		\"sample_interval\" : %d,\r\n", (int)bin->settings.app.sample_interval);

   i += sprintf (&json[i], "		\"sample_delay\" : %d,\r\n", (int)bin->settings.app.sample_delay);


   i += sprintf (&json[i], "		\"output_type\" : %d,\r\n", bin->settings.hal.output_type);

   i += sprintf (&json[i], "		\"sensor_pwr\" : %d,\r\n", bin->settings.hal.sensor_pwr);

   i += sprintf (&json[i], "		\"BUS_vID\" : %d,\r\n", bin->settings.hal.BUS_vID);

   i += sprintf (&json[i], "		\"BUS_bunch_send\" : %d,\r\n", bin->settings.hal.BUS_bunch_send);

   i += sprintf (&json[i], "		\"volt_valve_low\" : %f,\r\n", bin->settings.hal.volt_valve_low);

   i += sprintf (&json[i], "		\"volt_valve_high\" : %f,\r\n", bin->settings.hal.volt_valve_high);

   i += sprintf (&json[i], "		\"volt_bat_low\" : %f,\r\n", bin->settings.hal.volt_bat_low);

   i += sprintf (&json[i], "		\"volt_bat_warn\" : %f,\r\n", bin->settings.hal.volt_bat_warn);

   i += sprintf (&json[i], "		\"time_valve_on\" : %d,\r\n", (int)bin->settings.hal.time_valve_on);

   i += sprintf (&json[i], "		\"time_valve_off\" : %d,\r\n", (int)bin->settings.hal.time_valve_off);

   i += sprintf (&json[i], "		\"timeout_valve\" : %d,\r\n", (int)bin->settings.hal.timeout_valve);

   i += sprintf (&json[i], "		\"sleep_policy\" : %d,\r\n", bin->settings.hal.sleep_policy);

   i += sprintf (&json[i], "		\"policy_magic\": %u,\r\n", bin->policy.magic);

   i += sprintf (&json[i], "		\"PID\": %u,\r\n", bin->policy.PID);

   i += sprintf (&json[i], "		\"lock_sens2usr\": %d,\r\n", bin->policy.lock.sens2usr);

   i += sprintf (&json[i], "		\"lock_sens2device\": %d,\r\n", bin->policy.lock.sens2device);

   i += sprintf (&json[i], "		\"lock_auth2key\": %d,\r\n", bin->policy.lock.auth2key);

   i += sprintf (&json[i], "		\"lock_auth2usr\": %d,\r\n", bin->policy.lock.auth2usr);

   i += sprintf (&json[i], "		\"lock_auth2device\": %d,\r\n", bin->policy.lock.auth2device);

   i += sprintf (&json[i], "		\"log_login\": %d,\r\n", bin->policy.log.login);

   i += sprintf (&json[i], "		\"log_lock\": %d,\r\n", bin->policy.log.lock);

   i += sprintf (&json[i], "		\"log_begin\": %d,\r\n", bin->policy.log.begin);

   i += sprintf (&json[i], "		\"log_end\": %d,\r\n", bin->policy.log.end);

   i += sprintf (&json[i], "		\"log_credits\": %d,\r\n", bin->policy.log.credits);

   i += sprintf (&json[i], "		\"log_power_up\": %d,\r\n", bin->policy.log.power_up);

   i += sprintf (&json[i], "		\"log_power_dwn\": %d,\r\n", bin->policy.log.power_dwn);

   i += sprintf (&json[i], "		\"log_er_card_io\": %d,\r\n", bin->policy.log.er_card_io);

   i += sprintf (&json[i], "		\"log_er_sensor\": %d,\r\n", bin->policy.log.er_sensor);

   i += sprintf (&json[i], "		\"log_er_cons_on\": %d,\r\n", bin->policy.log.er_cons_on);

   i += sprintf (&json[i], "		\"log_er_cons_off\": %d,\r\n", bin->policy.log.er_cons_off);

   i += sprintf (&json[i], "		\"log_er_auth\": %d,\r\n", bin->policy.log.er_auth);

   i += sprintf (&json[i], "		\"log_er_valve_volt\": %d,\r\n", bin->policy.log.er_valve_volt);

   i += sprintf (&json[i], "		\"log_er_battery\": %d,\r\n", bin->policy.log.er_battery);

   i += sprintf (&json[i], "		\"log_er_memory\": %d,\r\n", bin->policy.log.er_memory);

   i += sprintf (&json[i], "		\"log_er_system\": %d\r\n", bin->policy.log.er_system);

   i += sprintf (&json[i], "}\r\n");

   return i;
}





/*!
 * \brief
 *    Convert a JSON log file to binary log file
 * \param json    Pointer to string containing the file PATH
 * \param size    The size of json input file
 * \param bin     Pointer to binary seed data
 * \return  The status of the operation
 *    \arg  0     Fail
 *    \arg  !0    Success, the number of output converted bytes
 */
static int log_json2bin (const char *json, size_t size, log_t *bin)
{
// 	unsigned int cur_conv_bytes = 0;
	int i, keys, k=0;

	// The gen_buffer_position contains the total number of the JSON data bytes to be converted.
   jsmn_init (&_parser);
   if ((keys = jsmn_parse (&_parser, json, size, _tokens, sizeof(_tokens)/sizeof(_tokens[0]))) < 0 ) {
	  return 1;
   }

   /* Loop over all keys of the root object */
   for (i=1; i<keys; ++i) {
	  // Log Data
	  if (_jsoneq(json, &_tokens[i], "timestamp") == 0) {
		  bin[k].timestamp = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "event") == 0) {
		  bin[k].event = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "id_type") == 0) {
		  bin[k].id.type = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "UID") == 0) {
		  bin[k].id.id.user.UID = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "CID") == 0) {
		  int array_token[CID_SIZE];
		  _json_get_array(array_token, CID_SIZE, json, (jsmntok_t *)_tokens, &i);
		  for (int j=0; j<CID_SIZE; ++j) {
			  bin[k].id.id.user.CID[j] = array_token[j];
		  }
	  }
	  else if (_jsoneq(json, &_tokens[i], "DID") == 0) {
		  int array_token[DID_SIZE];
		  _json_get_array(array_token, DID_SIZE, json, (jsmntok_t *)_tokens, &i);
		  if (bin[k].id.type) {
			  for (int j=0; j<DID_SIZE; ++j) {
				  bin[k].id.id.device.DID[j] = array_token[j];
			  }
		  }
	  }
	  else if (_jsoneq(json, &_tokens[i], "credits_lit") == 0) {
		  bin[k].credits.lit = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "credits_kwh") == 0) {
		  bin[k].credits.kwh = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
	  else if (_jsoneq(json, &_tokens[i], "credits_t") == 0) {
		  bin[k].credits.t = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
		  ++k;
	  }
	  else if (_jsoneq(json, &_tokens[i], "{") == 0) {
		  _json_skip_nested (json, (jsmntok_t *)_tokens, &i);
	  }
	  else
		 break;
   }
   return (sizeof(log_t)*k);
}





/*!
 * \brief
 *    Convert a binary log file to JSON log file.
 *    This function will convert the current packet from bin to JSON format.
 * \param json    Pointer to string containing the file PATH
 * \param bin     Pointer to binary seed data
 * \return
 *    \arg  0     Fail
 *    \arg  !0    Success, the number of output converted bytes
 */
static int log_bin2json (char *json, const log_t *bin)
{
	// Now we have saved the whole data and we can convert to JSON format (tags, fields, info, etc) one by one each binary equivalent datum.

   int i=0;

   // Currently, we have a total of 30 variables-structure members.

   i += sprintf (&json[i], "{\r\n");

   i += sprintf (&json[i], "		\"timestamp\" : %d,\r\n", (int)bin->timestamp);

   i += sprintf (&json[i], "		\"event\" : %d,\r\n", bin->event);

   i += sprintf (&json[i], "		\"id_type\" : %d,\r\n", bin->id.type);

   i += sprintf (&json[i], "		\"UID\" : %u,\r\n", bin->id.id.user.UID);

   i += sprintf (&json[i], "		\"CID\" : [");
   for (int j=0; j<sizeof(uint8_t)*(CID_SIZE-1); ++j) {
   	   i += sprintf (&json[i], "%d, ", bin->id.id.user.CID[j]);
   }
   i += sprintf (&json[i], "%d", bin->id.id.user.CID[sizeof(uint8_t)*(CID_SIZE-1)]);
   i += sprintf (&json[i], "],\r\n");

   i += sprintf (&json[i], "		\"DID\" : [");
   for (int j=0; j<sizeof(uint8_t)*(DID_SIZE-1); ++j) {
	   i += sprintf (&json[i], "%d, ", bin->id.id.device.DID[j]);
   }
   i += sprintf (&json[i], "%d", bin->id.id.device.DID[sizeof(uint8_t)*(DID_SIZE-1)]);
   i += sprintf (&json[i], "],\r\n");

   i += sprintf (&json[i], "		\"credits_lit\" : %u,\r\n", bin->credits.lit);

   i += sprintf (&json[i], "		\"credits_kwh\" : %u,\r\n", bin->credits.kwh);

   i += sprintf (&json[i], "		\"credits_t\" : %d\r\n", (int)bin->credits.t);

   i += sprintf (&json[i], "}");

   return i;
}





/*!
 * \brief
 *    Convert a JSON netstatus file to binary netstatus file
 * \param json    Pointer to string containing the file PATH
 * \param size    The size of json input file
 * \param bin     Pointer to binary seed data
 * \return  The status of the operation
 *    \arg  0     Fail
 *    \arg  !0    Success, the number of output converted bytes
 */
static int netstatus_json2bin (const char *json, size_t size, net_status_t *bin)
{
// 	unsigned int cur_conv_bytes = 0;
	int i, keys, dev_num=0;

	// The gen_buffer_position contains the total number of the JSON data bytes to be converted.
   jsmn_init (&_parser);
   if ((keys = jsmn_parse (&_parser, json, size, _tokens, sizeof(_tokens)/sizeof(_tokens[0]))) < 0 ) {
	  return 1;
   }

   /* Loop over all keys of the root object */
   for (i=1; i<keys; ++i) {
	  // Log Data
	  if (_jsoneq(json, &_tokens[i], "router") == 0) {
		  int array_token[RTP_ID_SIZE];
		  _json_get_array(array_token, RTP_ID_SIZE, json, (jsmntok_t *)_tokens, &i);
		  for (int j=0; j<RTP_ID_SIZE; ++j) {
			  bin->router.octet[j] = array_token[j];
		  }
	  }
	  else {
		  char device_name[11];
		  sprintf (device_name, "device%d", dev_num+1);
		  if (_jsoneq(json, &_tokens[i], device_name) == 0) {
			  int array_token[RTP_ID_SIZE];
			  _json_get_array(array_token, RTP_ID_SIZE, json, (jsmntok_t *)_tokens, &i);
			  for (int j=0; j<RTP_ID_SIZE; ++j) {
				  bin->dev[dev_num].octet[j] = array_token[j];
			  }
			  dev_num++;
		  }
		  else
			  break;
	  }
   }
   return (sizeof(net_status_t));
}





/*!
 * \brief
 *    Convert a binary netstatus file to JSON netstatus file.
 *    This function will convert the current packet from bin to JSON format.
 * \param json    Pointer to string containing the file PATH
 * \param bin     Pointer to binary seed data
 * \return
 *    \arg  0     Fail
 *    \arg  !0    Success, the number of output converted bytes
 */
static int netstatus_bin2json (char *json, const net_status_t *bin)
{
	// Now we have saved the whole data and we can convert to JSON format (tags, fields, info, etc) one by one each binary equivalent datum.

   int i=0;

   // Currently, we have a total of 30 variables-structure members.

   i += sprintf (&json[i], "{\r\n");

   i += sprintf (&json[i], "		\"router\" : [");
   for (int j=0; j<sizeof(uint8_t)*(RTP_ID_SIZE-1); ++j) {
   	   i += sprintf (&json[i], "%d, ", bin->router.octet[j]);
   }
   i += sprintf (&json[i], "%d", bin->router.octet[sizeof(uint8_t)*(RTP_ID_SIZE-1)]);
   i += sprintf (&json[i], "],\r\n");


   for (int dev_num=0; dev_num<GSM2M_MAX_CON_DEVICES-1; ++dev_num) {
	   i += sprintf (&json[i], "		\"device%d\" : [", dev_num+1);
	   for (int j=0; j<sizeof(uint8_t)*(RTP_ID_SIZE-1); ++j) {
		   i += sprintf (&json[i], "%d, ", bin->dev[dev_num].octet[j]);
	   }
	   i += sprintf (&json[i], "%d", bin->dev[dev_num].octet[sizeof(uint8_t)*(RTP_ID_SIZE-1)]);
	   i += sprintf (&json[i], "],\r\n");
   }

   i += sprintf (&json[i], "		\"device%d\" : [", GSM2M_MAX_CON_DEVICES);
   for (int j=0; j<sizeof(uint8_t)*(RTP_ID_SIZE-1); ++j) {
	   i += sprintf (&json[i], "%d, ", bin->dev[GSM2M_MAX_CON_DEVICES-1].octet[j]);
   }
   i += sprintf (&json[i], "%d", bin->dev[GSM2M_MAX_CON_DEVICES-1].octet[sizeof(uint8_t)*(RTP_ID_SIZE-1)]);
   i += sprintf (&json[i], "]\r\n");

   i += sprintf (&json[i], "}");

   return i;
}





/*!
 * \brief
 *    Convert a JSON gsm settings file to binary gsm settings file
 * \param json    Pointer to string containing the file PATH
 * \param size    The size of json input file
 * \param bin     Pointer to binary seed data
 * \return  The status of the operation
 *    \arg  0     Fail
 *    \arg  !0    Success, the number of output converted bytes
 */
static int GSM_settings_json2bin (const char *json, size_t size, GSM_settings_t *bin)
{
// 	unsigned int cur_conv_bytes = 0;
	int i, keys;

	// The gen_buffer_position contains the total number of the JSON data bytes to be converted.
   jsmn_init (&_parser);
   if ((keys = jsmn_parse (&_parser, json, size, _tokens, sizeof(_tokens)/sizeof(_tokens[0]))) < 0 ) {
	  return 1;
   }

   /* Loop over all keys of the root object */
   for (i=1; i<keys; ++i) {
	  if (_jsoneq(json, &_tokens[i], "vID") == 0) {
		  bin->vID = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }

	  else if (_jsoneq(json, &_tokens[i], "run_policy") == 0) {
		  bin->run_policy = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }

	  else if (_jsoneq(json, &_tokens[i], "report_interval") == 0) {
		  bin->report_interval = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }

	  else if (_jsoneq(json, &_tokens[i], "inc_time_window") == 0) {
		  bin->inc_time_window = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }

      else if (_jsoneq (json, &_tokens[i], "gsm_pin") == 0) {
         if (_json_get_string (bin->gsm_pin, json, (jsmntok_t *)_tokens, &i) == 0)
            return 1;
      }

      else if (_jsoneq (json, &_tokens[i], "apn") == 0) {
         if (_json_get_string (bin->apn, json, (jsmntok_t *)_tokens, &i) == 0)
            return 1;
      }

      else if (_jsoneq (json, &_tokens[i], "protocol") == 0) {
         if (_json_get_string (bin->protocol, json, (jsmntok_t *)_tokens, &i) == 0)
            return 1;
      }

      else if (_jsoneq (json, &_tokens[i], "server_domain") == 0) {
         if (_json_get_string (bin->server_domain, json, (jsmntok_t *)_tokens, &i) == 0)
            return 1;
      }

      else if (_jsoneq (json, &_tokens[i], "server_port") == 0) {
         if (_json_get_string (bin->server_port, json, (jsmntok_t *)_tokens, &i) == 0)
            return 1;
      }

      else if (_jsoneq (json, &_tokens[i], "time_server") == 0) {
         if (_json_get_string (bin->time_server, json, (jsmntok_t *)_tokens, &i) == 0)
            return 1;
      }

	  else if (_jsoneq(json, &_tokens[i], "ack_val") == 0) {
		  bin->acq.val = _json_get_primitive (json, (jsmntok_t *)_tokens, &i);
	  }
//	  else if (_jsoneq (json, &_tokens[i], "phone_number") == 0) {
//		 if (_json_get_string (bin->phone_number, json, (jsmntok_t *)_tokens, &i) == 0)
//		    return 1;
//	  }

	  else
		  break;
   }
   return (sizeof(GSM_settings_t));
}





/*!
 * \brief
 *    Convert a binary gsm settings file to JSON gsm settings file.
 *    This function will convert the current packet from bin to JSON format.
 * \param json    Pointer to string containing the file PATH
 * \param bin     Pointer to binary seed data
 * \return
 *    \arg  0     Fail
 *    \arg  !0    Success, the number of output converted bytes
 */
static int GSM_settings_bin2json (char *json, const GSM_settings_t *bin)
{
	// Now we have saved the whole data and we can convert to JSON format (tags, fields, info, etc) one by one each binary equivalent datum.

   int i=0;

   // Currently, we have a total of 30 variables-structure members.

   i += sprintf (&json[i], "{\r\n");

   i += sprintf (&json[i], "		\"vID\" : %d,\r\n", bin->vID);

   i += sprintf (&json[i], "		\"run_policy\" : %d,\r\n", bin->run_policy);

   i += sprintf (&json[i], "		\"report_interval\" : %d,\r\n", (int)bin->report_interval);

   i += sprintf (&json[i], "		\"inc_time_window\" : %d,\r\n", (int)bin->inc_time_window);


   i += sprintf (&json[i], "		\"gsm_pin\" : \"");
   for (int j=0; j<4; ++j) {
	   i += sprintf (&json[i], "%c", bin->gsm_pin[j]);
   }
   i += sprintf (&json[i], "\",\r\n");


   i += sprintf (&json[i], "		\"apn\" : \"");
   for (int j=0; j<49; ++j) {
	   i += sprintf (&json[i], "%c", bin->apn[j]);
   }
   i += sprintf (&json[i], "\",\r\n");


   i += sprintf (&json[i], "		\"protocol\" : \"");
   for (int j=0; j<3; ++j) {
	   i += sprintf (&json[i], "%c", bin->protocol[j]);
   }
   i += sprintf (&json[i], "\",\r\n");


   i += sprintf (&json[i], "		\"server_domain\" : \"");
   for (int j=0; j<49; ++j) {
	   i += sprintf (&json[i], "%c", bin->server_domain[j]);
   }
   i += sprintf (&json[i], "\",\r\n");


   i += sprintf (&json[i], "		\"server_port\" : \"");
   for (int j=0; j<5; ++j) {
	   i += sprintf (&json[i], "%c", bin->server_port[j]);
   }
   i += sprintf (&json[i], "\",\r\n");


   i += sprintf (&json[i], "		\"time_server\" : \"");
   for (int j=0; j<49; ++j) {
	   i += sprintf (&json[i], "%c", bin->time_server[j]);
   }
   i += sprintf (&json[i], "\",\r\n");


   i += sprintf (&json[i], "		\"ack_val\" : %d,\r\n", bin->acq.val);

//   i += sprintf (&json[i], "		\"phone_number\" : \"");
//   for (int j=0; j<19; ++j) {
//	   i += sprintf (&json[i], "%c", bin->phone_number[j]);
//   }
//   i += sprintf (&json[i], "\"\r\n");

   i += sprintf (&json[i], "}");

   return i;
}





/*
 * ================== Public API ==============
 */

// Rcom dummy interface
void rconv_clear_package (void) { UnivPackage.cursor = 0; }
void rconv_clear_struct (void)  { UnivStruct.cursor = 0; }

int rconv_tx (uint8_t *d, int s) {      	   /*!< RTP transmit */
   memcpy ((void*)&UnivPackage.buffer[UnivPackage.cursor],
           (const void*)d,
           s);
   UnivPackage.cursor += s;
   return s;
}
int rconv_rx (uint8_t *d, int s) {           /*!< RTP receive */
   memcpy ((void*)d,
           (const void*)&UnivPackage.buffer[UnivPackage.cursor],
           s);
   UnivPackage.cursor += s;
   return s;
}
/*!< RTP receive check */
int rconv_rx_check (void) { return INT_MAX; }
void rconv_rx_flush (void) { return; }


rtp_status_en rconv_structs_cb (rcom_rw_en rw, rcom_action_en act, uint8_t* data, msg_size_t size) {
   memcpy ((void*)&UnivStruct.data.buffer[UnivStruct.cursor],
           (const void*)data,
           size);
   UnivStruct.cursor += size;
   return RTP_OK;
}

/*!
 * \return
 *    \arg  0     Fail
 *    \arg  !0    Success
 */
int fs_clear_file (const char* fname) {
   FILE *f;

   if ((f = fopen (fname, "wb")) == NULL )
	   return 0;
   fclose (f);
   return 1;
}

/*!
 * \brief
 *    Load a file from disk in to buffer and return
 *    a pointer to buffer and the size
 * \param fname   Pointer to string containing the file PATH
 * \param data    Pointer to data buffer to write
 * \return
 *    \arg  0     Fail
 *    \arg  !0    Success, the number of read bytes
 */
int fs_load_file (const char* fname, char* data)
{
   FILE *f;
   long ss, _ss;

   if ((f = fopen (fname, "rb")) == NULL )
      return 0;
   // obtain file size:
   fseek (f , 0 , SEEK_END);
   ss = ftell (f);
   rewind (f);

   // copy the file into the buffer and close the file
   if ((_ss = fread ((void*)data, sizeof(uint8_t), ss, f)) > ss)
      _ss = 0;

   fclose (f);
   return _ss;
}


/*!
 * \brief
 *    Save a file to disk from a buffer and return
 *    the number of bytes written
 * \param fname   Pointer to string containing the file PATH
 * \param fb      Pointer to buffer
 * \param size    Size of buffer
 * \param ap	  Flag for append
 * \return
 *    \arg  0     Fail
 *    \arg  !0    Success, the number of written bytes
 */
int fs_save_file (char* fname, const char* fb, size_t size, uint8_t ap)
{
   FILE *f;
   long ws;

   if (ap != 0) {
	   if ((f = fopen (fname, "ab")) == NULL )
		   return 0;
   }
   else {
	   if ((f = fopen (fname, "wb")) == NULL )
		   return 0;
   }

   // copy the buffer into the file and close the file
   if ((ws = fwrite (fb , sizeof(char), size, f)) != size)
      ws = 0;
   fclose (f);
   return ws;
}

/*!
 * \brief
 *    Stream a file to stdout from a buffer and return
 *    the number of bytes streamed
 * \param fb      Pointer to buffer
 * \param size    Size of buffer
 * \return
 *    \arg  0     Fail
 *    \arg  !0    Success, the number of streamed bytes
 */
int fs_sream_file (const char* fb, size_t size)
{
   // stream the buffer stdout
   for (int i=0 ; i<size ; ++i)
      putchar (*fb++);
   return size;
}

/*!
 * \brief
 *    Convert a binary to JSON file.
 *    This function will convert the current packet from bin to JSON format.
 * \param json    Pointer to string buffer containing the json file
 * \param bin     Pointer to binary data
 * \return
 *    \arg  0     Fail
 *    \arg  !0    Success, the number of output converted bytes
 */
int gen_bin2json (char *json, const void* bin) {
   uint32_t log_objs, size = 0;

   switch (Input.ft) {

      case FT_SETTINGS:
         return settings_bin2json (json, (const Gen_Settings_t*)bin);

      case FT_GSM_SETTINGS:
    	 return GSM_settings_bin2json (json, (const GSM_settings_t*)bin);

      case FT_LOG:
    	  log_objs = UnivStruct.cursor / sizeof (log_t);
    	  size += sprintf (&json[size], "{\r\n");
    	  for (int i=0; i<log_objs; ++i) {
    		  size += log_bin2json(&json[size], &((const log_t*)bin)[i]);
    		  if (i==log_objs-1)
    			  size += sprintf (&json[size], "\r\n");
    		  else
    			  size += sprintf (&json[size], ",\r\n");
    	  }
    	  size += sprintf (&json[size], "}\r\n");
    	  return size;

      case FT_NETSTATUS:
    	  return netstatus_bin2json (json, (const net_status_t*)bin);

      case FT_ALL:
      case FT_NONE:
         return 0;
   }
   return 0;
}


/*!
 * \brief
 *    Convert a JSON file to binary
 * \param json    Pointer to string buffer containing the json file
 * \param size    The size of json input file
 * \param bin     Pointer to binary data
 * \return  The status of the operation
 *    \arg  0     Fail
 *    \arg  !0    Success, the number of output converted bytes
 */
int gen_json2bin (const char *json, size_t size, void* bin) {
   switch (Input.ft) {

      case FT_SETTINGS:
         return settings_json2bin (json, size, (Gen_Settings_t*)bin);

      case FT_GSM_SETTINGS:
         return GSM_settings_json2bin (json, size, (GSM_settings_t*)bin);

      case FT_LOG:
    	  return log_json2bin (json, size, (log_t*)bin);

      case FT_NETSTATUS:
    	  return netstatus_json2bin (json, size, (net_status_t*)bin);

      case FT_ALL:
      case FT_NONE:
         return 0;
   }
   return 0;
}
