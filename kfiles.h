/*!
 * \file kfiles.h
 * \brief
 *    CLI json file interface header
 */

#ifndef __kfiles_h__
#define __kfiles_h__

#ifdef __cplusplus
extern "C" {
#endif

#include <jsmn.h>
#include <rcom.h>
#include <kcli_impl.h>
#include <stdio.h>

/*
 * ============= User defines ==========
 */
#define _SEED_BUFFER_SIZE           (128)
#define _MDATA_BUFFER_SIZE		    (512)
#define _PACKET_SIZE				(256)
#define _PAYLOAD_SIZE				(251)
#define _JSON_FORMAT_FACTOR			(3)

#define _SETTINGS_BUFFER_SIZE       (256)
#define _TOKENS_SIZE				(512)
//#define etc...





/*
 * ========== key file ============
 */
extern rtp_t         rtp;
extern rcom_t        rcom;

// Rcom dummy interface
void rconv_clear_package (void);
void rconv_clear_struct (void);
int rconv_tx (uint8_t *d, int s);
int rconv_rx (uint8_t *d, int s);
int rconv_rx_check (void);
void rconv_rx_flush (void);

rtp_status_en rconv_structs_cb (rcom_rw_en rw, rcom_action_en act, uint8_t* data, msg_size_t size);

int fs_clear_file (const char* fname);
int fs_load_file (const char* fname, char* data);
int fs_save_file (char* fname, const char* fb, size_t size, uint8_t ap);
int fs_sream_file (const char* fb, size_t size);

int gen_bin2json (char *json, const void* bin);
int gen_json2bin (const char *json, size_t size, void* bin);

#ifdef __cplusplus
}
#endif

#endif /* #ifndef __kfiles_h__ */
