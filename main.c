/*!
 * \file main.c
 * \brief
 *    Main key cli application file
 */

#include <ui.h>
#include <kfiles.h>
#include <stdio.h>
#include <toolbox.h>


kcli_input_t   Input;

rtp_id_t john_doe  = { .octet = {1,2,3,4,5,6,7,8,9,10,11,12} };

#define  _verb(_msg_)            \
do {                             \
   if (Input.verb !=0 ) {        \
      printf (_msg_);            \
      printf ("\n");             \
   }                             \
} while (0)


#define  _done()     printf ("Done.\n")



void init (void) {
   // This is server
   rtp_id_t this  = { .octet = {0,0,0,0,0,0,0,0,0,0,0,0} };
//	rtp_id_t this  = { .octet = {1,2,3,4,5,6,7,8,9,10,11,12} };

   // make a dummy hardware
   rtp_link_tx (&rtp, rconv_tx);
   rtp_link_rx (&rtp, rconv_rx);
   rtp_link_rx_check (&rtp, rconv_rx_check);
   rtp_link_rx_flush (&rtp, rconv_rx_flush);
   rtp_set_br (&rtp, 9600);
   rtp_set_id (&rtp, &this);

   // link it with rcom for translation
   rtp_link_me_callback  (&rtp, (rtp_rx_callback_ft)rcom_receive_callback, (void*)&rcom);
   rtp_init (&rtp);

   // dummy rcom initialization
   rcom_link_rtp (&rcom, &rtp);
   rcom_link_rx_callback (&rcom, rconv_structs_cb);

   rcom_init(&rcom);
}


int main (int argc, char *argv[])
{
   size_t size;
   size_t rcom_pack_size;
   rcom_status_en st;

   // Init Dummy rcom
   init ();

   // Read command line input
   memset ((void*)&Input, 0, sizeof (kcli_input_t));
   if (parse_args (&Input, argc, argv) != 0)
      return 1;

   if (Input.output[0] == 0) {
      Input.verb = 0;   // Make sure verbose is off when we write to stdout
   }

   // Functionality dispatch
   switch (Input.func) {
      case FUN_B2J:
		 if (Input.input[0] == 0) {
			fprintf (stderr, "Error: Input file not specified [Exit]\n");
			return 1;
		 }
    	 if (Input.mode != INVALID)
    		 fprintf (stderr, "Warn: Input action identifier ignored.\n");
    	 if (Input.have_did)
    		 fprintf (stderr, "Warn: Input destination id ignored.\n");
         // Load RCOM local file.
         if ((rcom_pack_size = fs_load_file (Input.input, (char*)UnivPackage.buffer)) == 0) {
            fprintf (stderr, "Error: Can not read from %s [Exit]\n", Input.input);
            return 1;
         }
         _verb ("Load RCOM file [OK]");

		 if (Input.ft != FT_ALL) {
	         // Read packages and convert to binary inner format.
	         do {
	        	 st = rcom_listen (&rcom, 1);
				 if (st != RCOM_OK && st != RCOM_STOP) {
					fprintf (stderr, "Error: Can not read packages from %s [Exit]\n", Input.input);
					return 1;
				 }
	         } while (UnivPackage.cursor < rcom_pack_size);

			 _verb ("Load RCOM package/packages [OK]");

			 // Convert RCOM to JSON
			 if ((size = gen_bin2json (JsonFile, (const void*)UnivStruct.data.buffer)) == 0) {
				fprintf (stderr, "Error: Can not convert to JSON [Exit]\n");
				return 1;
			 }
			 _verb ("Convert RCOM to JSON file [OK]");

			 // Save/Output data
			 if (Input.output[0] != 0) {
				if (fs_save_file (Input.output, JsonFile, size, 0) == 0) {
				   fprintf (stderr, "Error: Can not write %s to disk [Exit]\n", Input.output);
				   return 1;
				}
				_verb ("Save JSON file [OK]");
			 }
			 else {
				fs_sream_file ((const char*)JsonFile, size);
				_verb ("Output JSON file [OK]");
			 }
			 rconv_clear_struct ();
		 }
		 else {
			 fs_clear_file (Input.output);	// Clear the file first
			 for (int dev=0; dev<GSM2M_MAX_CON_DEVICES; ++dev) {
				 for ((dev==0) ? (Input.ft = FT_NETSTATUS) : (Input.ft = FT_SETTINGS); Input.ft<=FT_LOG ; ++Input.ft) {
					 do {
						 st = rcom_listen (&rcom, 1);
						 if (st != RCOM_OK && st != RCOM_STOP) {
							fprintf (stderr, "Error: Can not read packages from %s [Exit]\n", Input.input);
							return 1;
						 }
					 } while ((Input.ft == FT_LOG) && (UnivPackage.cursor < rcom_pack_size));

					 // Convert RCOM to JSON
					 if ((size = gen_bin2json (JsonFile, (const void*)UnivStruct.data.buffer)) == 0) {
						fprintf (stderr, "Error: Can not convert to JSON [Exit]\n");
						return 1;
					 }
					 // Save/Output data
					 if (Input.output[0] != 0) {
						if (fs_save_file (Input.output, JsonFile, size, 1) == 0) {
						   fprintf (stderr, "Error: Can not write %s to disk [Exit]\n", Input.output);
						   return 1;
						}
					 }
					 else {
						fs_sream_file ((const char*)JsonFile, size);
					 }
					 rconv_clear_struct ();
				 }
			 }
			 _verb ("Convert RCOM to JSON file [OK]");
			 if (Input.output[0] != 0) {
				 _verb ("Save JSON file [OK]");
			 }
			 else {
				 _verb ("Output JSON file [OK]");
			 }
		 }
         rconv_clear_package ();
         _verb ("Done");
         break;

      case FUN_J2B:
    	 if (Input.mode == INVALID) {
    		 fprintf (stderr, "Error: Action identifier (READ or WRITE) not specified [Exit]\n");
    		 return 1;
    	 }
         if (!Input.have_did) {
            fprintf (stderr, "Error: Destination address not specified [Exit]\n");
            return 1;
         }

         switch (Input.ft) {
         	 case FT_SETTINGS:
         	 case FT_GSM_SETTINGS:
         		rcom.msg.action_com = RCOM_ACT_SETTINGS;
         		break;

         	 case FT_LOG:
         		rcom.msg.action_com = RCOM_ACT_LOG_DATA;
         		break;

         	 case FT_NETSTATUS:
         		rcom.msg.action_com = RCOM_ACT_NET_STATUS;
         		break;

         	 default:
         		 break;
         }

         switch (Input.mode) {
			 case WRITE:
				 if (Input.input[0] == 0) {
				    fprintf (stderr, "Error: Input file not specified [Exit]\n");
				    return 1;
				 }
				 rcom.msg.rw_com = RCOM_WRITE;
				 // Load JSON local file
				 if ((size = fs_load_file (Input.input, JsonFile)) == 0) {
					fprintf (stderr, "Error: Can not read from %s [Exit]\n", Input.input);
					return 1;
				 }
				 _verb ("Load JSON file [OK]");

				 // Parse JSON file and convert to binary inner format.
				 if ((size = gen_json2bin ((const char*)JsonFile, size, (void*)UnivStruct.data.buffer)) == 0) {
					fprintf (stderr, "Error: Can not parse json %s [Exit]\n", Input.input);
					return 1;
				 }
				 _verb ("Parse JSON file [OK]");
				 break;

			 case READ:
				 if (Input.input[0] != 0)
					 fprintf (stderr, "Warn: Input file %s, ignored.\n", Input.input);
				 rcom.msg.rw_com = RCOM_READ_REQUEST;
				 size = sizeof(uint8_t);
				 break;

			 case INVALID:
				 break;

			 default:
				 break;
         }

		 // Make packages and store them to Package buffer
		 rcom_send (&rcom, &Input.did, UnivStruct.data.buffer, size, 0);
		 _verb ("Convert to RCOM package/packages [OK]");

		 // Save/Output data
		 if (Input.output[0] != 0) {
			if (fs_save_file (Input.output, (const char*)UnivPackage.buffer, UnivPackage.cursor, 0) == 0) {
			   fprintf (stderr, "Error: Can not write %s to disk [Exit]\n", Input.output);
			   return 1;
			}
			_verb ("Save RCOM file [OK]");
		 }
		 else {
			fs_sream_file ((const char*)UnivPackage.buffer, UnivPackage.cursor);
			_verb ("Output RCOM file [OK]");
		 }
		 rconv_clear_struct ();
		 rconv_clear_package ();
		 _verb ("Done");
		 break;

      case FUN_NONE:
         break;
   }


   return 0;
}
